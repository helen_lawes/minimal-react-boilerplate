# Minimal react boilerplate

This includes react, styled-components, eslint, prettier, parcel, docz. Parcel is used as an application bundler and Docz is for documentation style guides.

## Scripts

### Dev

`npm start`

Website can be found running on http://localhost:3000

### Build

`npm run build`

Output of build will be found in the dist folder

### Documentation

`npm run docs`

Website for documentation style guide can be found running on http://localhost:3001

`npm run build-docs`

Output of documentation build will be found in the .docz/dist folder
