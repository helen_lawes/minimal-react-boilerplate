import React from 'react';

import StyledApp from './App.styles';

const App = () => <StyledApp>App works!</StyledApp>;

export default App;
