import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('<App />', () => {
	test('Should render without error', () => {
		const component = shallow(<App />);

		expect(component).toMatchSnapshot();
	});
});
